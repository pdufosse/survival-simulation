# Simulation of survival data

This Python package can be used to simulate survival data.

/!\ _Research code, currently under development_ /!\

/!\ Utilisez le à vos risques et périls /!\

![](example_simulation.png)

## Installation

After cloning the repository

```
cd survival-simulation
python -m pip install .
```

## Content

The focus is to study the performance (both from inference and discrimination point of views) of survival model from simulations.
The ultimate goal is to support various mechanisms of: 

- missing data
- longitudinal data

This package is built to work around the following:

- `lifelines`
- `pycox`
- `sksurv`
- to some extent, `sklearn`

There are already simulation studies in `pycox`, but they are rather limited.
In particular, there are no *missing* neither *longitudinal* data.


### Metrics

Available metrics are:

- from `pycox` : brier score, negative binomial log-likelihood, time-dependent c-index
- from `sksurv` : inverse probability weighting using the KM censorship

## Usage

Compatible with the `lifelines` package.
See also `survivalsim.utils` functions to transform the dataset from `numpy` arrays to `pandas` and vice versa (should be improved in future releases).

## Roadmap

In order or priority (~ simplicity)

### Methodological improvements

- [ ] Add more metrics
- [ ] Better implement missing data
  - [ ] Implement new patterns
- [ ] Implement longitudinal data

### Code improvements

- `benchmark.py`
  - Duplicated code to fit and predict NNet models
- Implement a class to switch between data format of all libraries (pycox, sksurv, lifelines)

### Code testing

TODO

#### Static test using `mypy`

Run
```bash
python -m mypy survival-simulation/survivalsim --ignore-missing-imports
```

#### Runtime tests

TODO
## Contributing

To some extent, this is also my own workspace to experiment, so please create your own branch and propose a MR if you have changes to suggest.

## Authors and acknowledgment

Paul Dufossé

## License

Under the GNU GPLv3 license, see also `LICENSE` file.

## Project status

Ongoing development.
