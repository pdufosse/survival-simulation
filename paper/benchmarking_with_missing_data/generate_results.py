"""
Contains code to generate results data from paper

Algorithmes d'apprentissage pour les problèmes de survie avec données manquantes
Paul Dufossé, Sébastien Benzekry
"""
import io
from argparse import ArgumentParser
from contextlib import redirect_stderr, redirect_stdout
from pathlib import Path

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scienceplots
import torch
from pycox.preprocessing.label_transforms import LabTransDiscreteTime

import survivalsim as ss

output_dir = Path("output") / "tmp"
output_dir.mkdir(parents=True, exist_ok=True)

# Plotting options

plt.style.use("science")
mpl.rc("axes", prop_cycle=ss.plotting._colors_palette)

# Arguments

parser = ArgumentParser()
n = 500
d = 5  # number of covariables
cv_splits = 5
plot_data = True
hazards = "nph"
# censorship = "ci"
# setup
wshape = 2
missingness = ["mcar", "mnar"]


# Generate data
for pattern in missingness:
    if pattern == "mcar":
        ratios = [0.4, 0.6, 0.8]
    else:
        ratios = [0.62, 0.82, 1.03]
    for ratio in ratios:
        if hazards == "nph":
            if pattern == "mcar":
                sim_class = ss.SimNPH
                missing_params = {"ratio": ratio}
            elif pattern == "mnar":
                sim_class = ss.SimNPHMNAR
                missing_params = {"thresh": ratio}
            else:
                raise
            np.random.seed(12345678)
            torch.manual_seed(1)
            beta = np.array([0] + [0] * d + [1, -1, 2, 0] + [0] * 6)
        else:
            raise

        simulator = sim_class(d, beta, wshape=wshape)
        simulator.xsim.p = 0.5  # proportion in each group
        df, df_full = simulator.generate(n, plot=False, missing_params=missing_params)

        if plot_data:
            plt.figure(figsize=(3, 4))
            simulator.plot_survival(df, add_title=False)
            fig = plt.gcf()
            plt.savefig(
                output_dir / f"km_plot_of_simulated_data_{hazards}_n={n}_d={d}.pdf",
                format="pdf",
            )
            ss.plotting.plot_data_summary(df, add_suptitle=False)
            plt.savefig(
                output_dir
                / f"summary_plot_of_simulated_data_{hazards}_n={n}_d={d}.pdf",
                format="pdf",
            )

        X_full = df_full.filter(like="x").values
        X = df.filter(like="x").values
        T = df["time"]
        C = df["event"]

        labtrans = LabTransDiscreteTime(20, scheme="equidistant").fit(T, C)

        runners_imputers = (
            [
                (est, imp)
                for imp in [
                    ss.GroundTruth(),
                    ss.SimpleImputer(),
                    ss.SimpleImputer(add_indicator=True),
                    ss.MultipleImputer(),
                    ss.MultipleImputer(add_indicator=True),
                    ss.KNNImputer(),
                    ss.KNNImputer(add_indicator=True),
                ]
                for est in [(ss.CPHRunner(), {}), (ss.RSFRunner(), {})]
            ]
            + [
                (est, imp)
                for imp in [
                    ss.GroundTruth(),
                    ss.SimpleImputer(),
                    ss.MultipleImputer(),
                    ss.KNNImputer(),
                ]
                for est in [
                    (ss.DeepSurvRunner(ss.make_net(d)), {"batch_size": 54}),
                    (
                        ss.DeepHitRunner(
                            ss.make_net(d, out_features=labtrans.out_features), labtrans
                        ),
                        {"batch_size": 54},
                    ),
                ]
            ]
            + [
                (est, imp)
                for imp in [
                    ss.SimpleImputer(add_indicator=True),
                    ss.MultipleImputer(add_indicator=True),
                    ss.KNNImputer(add_indicator=True),
                ]
                for est in [
                    (ss.DeepSurvRunner(ss.make_net(2 * d)), {"batch_size": 54}),
                    (
                        ss.DeepHitRunner(
                            ss.make_net(2 * d, out_features=labtrans.out_features),
                            labtrans,
                        ),
                        {"batch_size": 54},
                    ),
                ]
            ]
        )
        print(runners_imputers)

        print(
            f"Generating results  for {pattern.upper()} pattern with parameter {ratio} ..."
        )
        for (runner, runner_args), imputer in runners_imputers:
            print(
                f"Now running algo {runner.__name__} with imputer {imputer.__name__} ..."
            )
            results = {}

            buff_stdout, buff_stderr = io.StringIO(), io.StringIO()

            with redirect_stdout(buff_stdout), redirect_stderr(buff_stderr):
                if imputer.__name__ == "ground_truth":
                    _X = X_full
                else:
                    _X = X
                results[imputer.__name__] = runner.run_cv(
                    _X, T, C, imputer=imputer, cv_splits=cv_splits, **runner_args
                )

            results = ss.merge_results(results)
            for k, v in missing_params.items():
                results[k] = v
            ss.write_results(
                simulator,
                output_dir,
                ss.dict_to_str(missing_params),
                runner,
                results,
                fn="results_" + imputer.__name__,
                log=buff_stdout,
                warnings=buff_stderr,
            )

        neumiss_runners = [
            [ss.DeepSurvRunner(ss.make_net(d, neumiss=True)), {"batch_size": 54}],
            [
                ss.DeepHitRunner(
                    ss.make_net(d, out_features=labtrans.out_features, neumiss=True),
                    labtrans,
                ),
                {"batch_size": 54},
            ],
        ]

        for runner, runner_args in neumiss_runners:
            print(f"Now running algo {runner.__name__} with NeuMiss imputer ...")
            results = {}
            runner.check = False
            buff_stdout, buff_stderr = io.StringIO(), io.StringIO()
            with redirect_stdout(buff_stdout), redirect_stderr(buff_stderr):
                results["neumiss"] = runner.run_cv(
                    X,
                    T,
                    C,
                    imputer=ss.GroundTruth(),
                    cv_splits=cv_splits,
                    **runner_args,
                )
            results = ss.merge_results(results)
            for k, v in missing_params.items():
                results[k] = v
            ss.write_results(
                simulator,
                output_dir,
                ss.dict_to_str(missing_params),
                runner,
                results,
                fn="results_neumiss",
                log=buff_stdout,
                warnings=buff_stderr,
            )
