import numpy as np
import survivalsim as ss
import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.rc("axes", prop_cycle=ss.plotting._colors_palette)

d = 5
n = 500
np.random.seed(123)
beta = np.array(
    [0]
    + [0] * d
    + [1, -1, 2, 0]
    + [0] * 6
)
simulator = ss.SimNPH(d, beta, wshape=2)
df, _ = simulator.generate(n, plot=False, missing_params={"ratio": 0})
plt.figure(figsize=(7, 5))
ss.plotting.plot_data_summary(df)
plt.savefig("example_simulation.png", format="png")
