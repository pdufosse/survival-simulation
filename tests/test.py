import pytest
import numpy as np
from survivalsim import BaseSimulator, BaseRunner, make_net, DeepSurvRunner, SimPH

n = 10
d = 5
X = BaseSimulator._gen_X(n, d)
X, y, _ = SimPH(d, np.ones(d)).generate(n)
net = make_net(d)
deepsurv = DeepSurvRunner(net)
deepsurv.fit(X, list(y.T))
surv = deepsurv.predict(X)


def test_gen_X():
    assert X.shape == (n, d)


@pytest.mark.xfail(raises=ValueError)
def test_check_for_nans():
    _X = SimPH(d, np.ones(d)).generate(n)
    _X[0, 0] = np.nan
    BaseRunner.check_for_nans(n)


def test_deepsurv():
    assert surv.shape[1] == n
