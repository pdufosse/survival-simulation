from cycler import cycler
from lifelines import KaplanMeierFitter
from lifelines.plotting import add_at_risk_counts
from pandas import DataFrame
from survivalsim.metrics import bootstrap_estimate_of_concordance
from typing import Union, Callable, Optional
import matplotlib as mpl
import matplotlib.pyplot as plt
import os
import numpy as np
import pandas as pd
import seaborn as sns


_colors_palette = cycler(
    color=("#9BAAAF", "#11304F", "#989B6C",
           "#3F4738", "#195972", "#4D251D"))


km_plot_options = {
    "show_censors": True,
    "censor_styles": {"marker": "|"}
}


def use_mplstyle(plot_fun: Callable):
    def wrapper(*args, **kwargs):
        with mpl.style.context(os.path.join(os.path.split(__file__)[0], '_mpl_config', 'beamer.mplstyle')):
            plot_fun(*args, **kwargs)
    return wrapper


def plot_km(
        T: Union[np.ndarray, pd.Series],
        C: Union[np.ndarray, pd.Series],
        label: Optional[str] = None,
        ax: mpl.axes.Axes = None):
    kmf = KaplanMeierFitter()
    ax = (kmf
          .fit(T, C, label=label)
          .plot_survival_function(ax=ax, **km_plot_options))
    return kmf, ax


@use_mplstyle
def plot_survival(
        df: pd.DataFrame,
        by: Optional[str] = "group",
        add_title: bool = True,
        add_risk_table: bool = True,
        ax: mpl.axes.Axes = None):

    if ax is None:
        ax = plt.gca()

    if by is not None:
        km_estimators = []
        for val in sorted(df[by].unique()):
            ix = df['group'] == val
            kmf, ax = plot_km(
                df.loc[ix]['time'],
                df.loc[ix]['event'],
                label=str(val), ax=ax)
            km_estimators.append(kmf)
    else:
        kmf, ax = plot_km(
            df.loc['time'],
            df.loc['event'],
            ax=ax)
        km_estimators = [kmf]

    ax.legend(title="Group")
    ax.grid(True)
    ax.set_xlabel(r"$T$")
    ax.set_ylabel(r"Survived until $T$")
    ax.set_ylim(0, 1)

    if add_risk_table:
        add_at_risk_counts(*km_estimators, ax=ax)
    if add_title:
        plt.suptitle("KM curves of survival")
    plt.tight_layout()
    return ax


@use_mplstyle
def plot_censorship(
        df: pd.DataFrame,
        by: Optional[str] = "group",
        add_title: bool = True,
        add_risk_table: bool = True,
        ax: mpl.axes.Axes = None):

    if ax is None:
        ax = plt.gca()

    if by is not None:
        km_estimators = []
        for val in df[by].unique():
            ix = df['group'] == 0
            ax, kmf = plot_km(df.loc[ix]['time'],
                              1 - df.loc[ix]['event'],
                              label=str(val), ax=ax)
            km_estimators.append(kmf)
    else:
        kmf, ax = plot_km(
            df.loc['time'],
            1 - df.loc['event'],
            ax=ax)
        km_estimators = [kmf]

    ax.legend(title="Group")
    ax.grid(True)
    ax.set_xlabel(r"T")
    ax.set_ylabel(r"Observed until $T$")
    ax.set_ylim(0, 1)

    if add_risk_table:
        add_at_risk_counts(*km_estimators, ax=ax)
    if add_title:
        plt.suptitle("KM curves of censorship")
    plt.tight_layout()

    return ax


@use_mplstyle
def plot_data_summary(
        df: DataFrame,
        add_suptitle: bool = True):
    fig, axd = plt.subplot_mosaic(
        [['left', 'upper right'],
         ['left', 'lower right']],
        figsize=(5.5, 3.2),
        layout="constrained")
# for k in axd:
#     annotate_axes(axd[k], f'axd["{k}"]', fontsize=14)

    plt.sca(axd["left"])
    plot_survival(df, add_title=False, add_risk_table=False)
    plt.title("KM curves")

    axd["table left"] = fig.axes[-1]

    plt.sca(axd["upper right"])
    plt.grid(True)
    df["group"] = df["group"]
    sns.scatterplot(
        df, x="f", y="survival", hue="group")
    sns.regplot(df,
                x="f", y="survival", scatter=False, color="red")
    plt.xlabel("$f(X_i)$")
    plt.ylabel("True survival")
    plt.gca().get_legend().remove()
    plt.yscale("log")
    plt.title("Model values by group")

    plt.sca(axd["lower right"])
    plt.grid(True)
    sns.histplot(
        bootstrap_estimate_of_concordance(df)["exact"],
        kde=True, ax=axd['lower right'], label="Bootstrap samples")
    plt.xlabel("Concordance")
    plt.ylabel("Counts")
    plt.legend()
    plt.title("Oracle $C$ estimator")

    # lines_labels = [ax.get_legend_handles_labels() for ax in fig.axes]
    # lines, labels = [sum(lol, []) for lol in zip(*lines_labels)]
    # fig.legend(lines, labels, loc='lower center', ncol=len(lines),
    #                  bbox_to_anchor=(.5, -0.1), bbox_transform=plt.gcf().transFigure)
    plt.tight_layout()
    if add_suptitle:
        fig.suptitle('Characteristics of simulated data')
    plt.tight_layout()


@use_mplstyle
def plot_estimators_of_concordance(
        df: pd.DataFrame,
        add_title: bool = True,
        ax: mpl.axes.Axes = None):
    """_summary_

    Args:
        df (pd.DataFrame): Dataframe of results, contains"exact" and "harrell" columns.
        add_title (bool, optional): Whether to add title with median deviation. Defaults to True.
        ax (mpl.axes.Axes, optional): Axis to print figure on. Defaults to None.
    """
    if ax is None:
        _, ax = plt.figure(figsize=(3, 3))
    sns.scatterplot(data=df, x="exact", y="harrell")
    plt.xlabel(r"Oracle $C$")
    plt.ylabel(r"Harrell's $\hat{C}$")
    xymin = min(plt.xlim()[0], plt.ylim()[0])
    xymax = max(plt.xlim()[1], plt.ylim()[1])
    plt.plot([xymin, xymax], [xymin, xymax], "k-", zorder=-1, lw=.5)
    plt.xlim(xymin, xymax), plt.ylim(xymin, xymax)
    plt.axis("equal")
    plt.annotate(r"$C = \hat{C}$", (xymin + .01, xymin + .01))
    if add_title:
        plt.title(
            f"median($\hat{{C}} - C$) = {np.median(- np.array(df['exact']) + np.array(df['harrell'])):.4f}",)
    return ax
