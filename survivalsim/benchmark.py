import io
import warnings
from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import torchtuples as tt  # Some useful functions
from IPython.core.debugger import set_trace
from lifelines import CoxPHFitter
from lifelines.exceptions import ConvergenceError
from neumiss import NeuMissBlock  # type: ignore
from pycox.evaluation import EvalSurv
from pycox.models import CoxPH, DeepHitSingle
from pycox.preprocessing.label_transforms import LabTransDiscreteTime
from sklearn.model_selection import KFold, train_test_split
from sksurv.ensemble import RandomSurvivalForest
from sksurv.metrics import concordance_index_censored, concordance_index_ipcw
from torch import nn

from survivalsim import impute as ssimp
from survivalsim import utils
from survivalsim.metrics import estimate_concordance
from survivalsim.simulate import BaseSimulator

# from lifelines.utils.sklearn_adapter import sklearn_adapter


_default_optimizer = tt.optim.Adam  # type: ignore


class BaseRunner:
    """Base class to run survival experiments.

    Implements: CV, metrics computation
    To add a model, inherit and overwrites fit and predict methods
    """

    def __init__(self, random_state: int = 0):
        """Initialization common to all models.

        Should be called through `super()` in child classes.

        Args:
            random_state (int, optional): Random seed passed to computational libraries or `self.set_seed()`. Defaults to 0.
        """
        self.random_state = random_state
        self.check = True

    def set_seed(self):
        np.random.seed(self.random_state)
        torch.manual_seed(self.random_state)

    def fit(self, X, T, C):
        raise NotImplementedError

    def predict(self, X):
        raise NotImplementedError

    def check_for_nans(self, X: np.ndarray):
        if np.isnan(X).any():
            raise ValueError("NaNs in dataset.")

    @staticmethod
    def oracle_cv(self, df: pd.DataFrame, cv_splits: int = 10):
        kf = KFold(n_splits=cv_splits)
        results = {}
        for i, (train, test) in enumerate(kf.split(df)):
            results[i] = estimate_concordance(df.iloc[test])
        results = results_to_df(results)
        return results

    def run_cv(
        self,
        X,
        T,
        C,
        imputer: Optional[ssimp.Imputer] = None,
        cv_splits: int = 10,
        **kwargs,
    ):
        results = {}
        kf = KFold(n_splits=cv_splits)
        y = utils.target_to_cv(T, C)
        for i, (train, test) in enumerate(kf.split(X, y)):
            if imputer is not None:
                imputer.fit(X[train])
                _X_train = imputer.transform(X[train])
                _X_test = imputer.transform(X[test])
            self.fit(_X_train, y[train], **kwargs)
            surv_test = self.predict(_X_test)
            time_grid = surv_test.index.values
            t_for_C = utils.median(T)
            if t_for_C not in time_grid:
                t_for_C = utils.median(time_grid)
            results[i] = compute_metrics(
                surv_test, y[test], time_grid, t=t_for_C, y_train=y[train]
            )
        results = results_to_df(results)
        return results

    def run_train_test(self, X, T, C, simulator):
        raise NotImplementedError


class BaseNNRunner(BaseRunner):
    def _fit(self, X, y):
        raise NotImplementedError

    def _predict(self, X):
        raise NotImplementedError


class CPHRunner(BaseRunner):
    """Baseline (Cox PH) survival model"""

    __name__ = "cph"

    def __init__(self, *args):
        super().__init__(*args)
        self.model = CoxPHFitter()

    def fit(self, X, y):
        df = utils.cv_to_df(X, y)
        try:
            self.model.fit(df, "time", "event")
        except ConvergenceError:
            warnings.warn(
                "Convergence error in Cox model, adding a penalty and retry..."
            )
            self.model = CoxPHFitter(penalizer=0.1)
            self.model.fit(df, "time", "event")

    def predict(self, X):
        return self.model.predict_survival_function(X)


class RSFRunner(BaseRunner):
    """Random survival forest survival model

    Seed is set through `random_state` argument of `RandomSurvivalForest`.
    """

    __name__ = "rsf"

    def __init__(self, *args):
        super().__init__(*args)
        self.model = RandomSurvivalForest(
            n_estimators=100,
            min_samples_split=10,
            min_samples_leaf=15,
            n_jobs=1,
            random_state=self.random_state,
        )

    def fit(self, X, y):
        y = utils.array_to_structured(y)
        self.model.fit(X, y)

    def predict(self, X):
        preds = self.model.predict_survival_function(X)
        test = np.array([_.x for _ in preds])
        assert (test == test[0, :]).all()
        df = pd.DataFrame([_.y for _ in preds]).T.set_index(preds[0].x)
        return df


class DeepSurvRunner(BaseRunner):
    """DeepSurv (Cox PH + neural net) survival model"""

    __name__ = "deepsurv"

    def __init__(self, net, optimizer=_default_optimizer, *args):
        super().__init__(*args)
        self.net = net
        self.optimizer = optimizer
        self._init_model()

    def _init_model(self):
        self.model = CoxPH(self.net, self.optimizer)
        self.model.optimizer.set_lr(0.01)

    def fit(
        self,
        X,
        y,
        epochs: int = 512,
        batch_size: int = 256,
        val_size: float = 0.2,
        verbose: bool = True,
        reinit: bool = True,
    ):
        if self.check:
            self.check_for_nans(X)
        if reinit:
            self._init_model()
        self.set_seed()
        callbacks = [tt.callbacks.EarlyStopping()]  # type: ignore
        # data manipulation
        # to provide compliant structure & encoding
        data = train_test_split(X, y, test_size=val_size)
        X_train, X_val, y_train, y_val = list(map(utils.to_torch_float, data))
        # set_trace()
        y_train = list(y_train.T)
        y_val = list(y_val.T)
        log = self.model.fit(
            X_train,
            y_train,
            batch_size,
            epochs,
            callbacks,
            verbose,
            val_data=(X_val, y_val),
            val_batch_size=batch_size,
        )
        return log

    def predict(self, X):
        self.model.compute_baseline_hazards()
        return self.model.predict_surv_df(utils.to_torch_float(X))


class DeepHitRunner(BaseRunner):
    """DeepHit (flexible + neural net) survival model"""

    __name__ = "deephit"

    def __init__(
        self,
        net_discrete,
        labtrans: LabTransDiscreteTime,
        optimizer=_default_optimizer,
        alpha: float = 0.2,
        sigma: float = 0.1,
        *args,
    ):
        super().__init__(*args)
        self.net = net_discrete
        self.optimizer = optimizer
        self.labtrans = labtrans
        self.alpha = alpha
        self.sigma = sigma
        self._init_model()

    def _init_model(self):
        self.model = DeepHitSingle(
            self.net,
            self.optimizer,
            alpha=self.alpha,
            sigma=self.sigma,
            duration_index=self.labtrans.cuts,
        )
        self.model.optimizer.set_lr(0.01)

    def fit(
        self,
        X: np.ndarray,
        y: np.ndarray,
        epochs: int = 512,
        batch_size: int = 256,
        val_size: float = 0.2,
        verbose: bool = True,
        reinit: bool = True,
    ):
        if self.check:
            self.check_for_nans(X)
        if reinit:
            self._init_model()
        self.set_seed()
        callbacks = [tt.callbacks.EarlyStopping()]  # type: ignore
        data = train_test_split(X, y, test_size=val_size)
        X_train, X_val, y_train, y_val = list(map(utils.to_torch_float, data))
        y_train_discrete = self.labtrans.fit_transform(*y_train.T)
        y_val_discrete = self.labtrans.fit_transform(*y_val.T)
        log = self.model.fit(
            X_train,
            y_train_discrete,
            batch_size,
            epochs,
            callbacks,
            verbose,
            val_data=(X_val, y_val_discrete),
            val_batch_size=batch_size,
        )
        return log

    def predict(self, X, n_interpolate: int = 0):
        if n_interpolate > 0:
            self.model.interpolate(n_interpolate)
        return self.model.predict_surv_df(utils.to_torch_float(X))


def make_net(
    in_features: int,
    out_features: int = 1,  # pass labtrans.out_features for discrete models
    dropout: float = 0.1,
    output_bias: bool = False,
    neumiss: bool = False,
):
    neumiss_depth = 30

    net_components = [
        nn.Linear(in_features, 32),
        nn.ReLU(),
        nn.BatchNorm1d(32),
        nn.Dropout(dropout),
        nn.Linear(32, 32),
        nn.ReLU(),
        nn.BatchNorm1d(32),
        nn.Dropout(dropout),
        nn.Linear(32, out_features, output_bias),
    ]
    if neumiss:
        net_components = [NeuMissBlock(in_features, neumiss_depth)] + net_components
    return nn.Sequential(*net_components)


def compute_metrics(surv_test, y_test, time_grid, t=None, y_train=None):
    results = {}
    ev = EvalSurv(surv_test, *y_test.T, censor_surv="km")
    # from pycox
    results["Ctd"] = ev.concordance_td()
    results["iBS"] = ev.integrated_brier_score(time_grid)
    results["iNBLL"] = ev.integrated_nbll(time_grid)
    # from sksurv
    if t is not None:
        y_train_struct = utils.array_to_structured(y_train)
        y_test_struct = utils.array_to_structured(y_test)
        # sksurv requires risk not survival
        surv_estimate = 1 - surv_test.loc[t].values
        # set_trace()
        results["C"] = concordance_index_censored(
            y_test_struct["cens"], y_test_struct["time"], surv_estimate
        )[0]
        if y_train is not None:
            results["Cipcw"] = concordance_index_ipcw(
                y_train_struct,
                y_test_struct,
                surv_estimate,
                tau=y_train_struct["time"].max(),
            )[0]
    return results


def results_to_df(results: dict):
    df_results = pd.DataFrame.from_dict(results).T
    df_results.index.rename("isplit", inplace=True)
    return df_results


def merge_results(many_results: dict):
    df_results = pd.concat(many_results)
    df_results.index.rename("method", level=0, inplace=True)
    return df_results


def plot_results(any_result, concat=True, vars_to_show=("C", "Ctd", "Cipcw")):
    # TODO check if outdated
    if concat:
        df_results = merge_results(any_result)
        vars_to_pivot = ["method", "isplit"]
        hue = "method"
    else:
        df_results = any_result
        vars_to_pivot = ["isplit"]
        hue = None
    df_results = pd.melt(
        df_results.reset_index(), id_vars=vars_to_pivot, value_vars=vars_to_show
    )
    plt.grid(True, which="both")
    sns.boxplot(y="variable", x="value", hue=hue, data=df_results)
    plt.legend(bbox_to_anchor=(1.02, 0.5), loc="upper left", borderaxespad=0)


def write_results(
    simulator: BaseSimulator,
    output_dir: Path,
    prefix: str,
    runner: BaseRunner,
    df_results: pd.DataFrame,
    fn: str = "results",
    log: Optional[io.StringIO] = None,
    warnings: Optional[io.StringIO] = None,
):
    final_dir = output_dir / simulator.get_prefix() / prefix / runner.__name__
    final_dir.mkdir(parents=True, exist_ok=True)
    df_results.to_csv(final_dir / (fn + ".csv"))
    if log is not None:
        with open(final_dir / (fn + ".log"), mode="w") as f:
            f.write(log.getvalue())
    if warnings is not None:
        with open(final_dir / (fn + ".warnings"), mode="w") as f:
            f.write(warnings.getvalue())
