from pandas import DataFrame
from sksurv.metrics import concordance_index_censored
from typing import Optional
import numpy as np
import pycox.evaluation as pe


class Surv:

    def __init__(self, T, C):
        self.times = T
        self.events = C
        self.duration_col = "time"
        self.event_col = "event"

    def to_struct(self):
        raise NotImplementedError

    def to_df(self):
        raise NotImplementedError

    def to_y(self):
        return (self.times, self.events)


class EvalSurv(pe.EvalSurv):

    def concordance(self):
        raise NotImplementedError


def bootstrap_estimate_of_concordance(df: DataFrame):
    results = {"exact": [], "harrell": []}
    for i in range(100):
        results["exact"].append(
            estimate_concordance(
                df.sample(100, replace=True), "survival", "f", None
            )
        )
        results["harrell"].append(
            estimate_concordance(
                df.sample(100, replace=True), "time", "f", "event"
            )
        )
    return DataFrame.from_dict(results)


def estimate_concordance(
        df: DataFrame,
        survival_col: str = "survival",
        model_col: str = "f",
        event_col: Optional[str] = None):
    if event_col is None:
        event_array = np.ones(len(df))
    else:
        event_array = df[event_col].values
    event_array = event_array.astype(bool)
    return concordance_index_censored(
        event_array, df[survival_col], - df[model_col]
    )[0]
