from typing import Union

import numpy as np
import pandas as pd


def missing_completely_at_random(df: pd.DataFrame, ratio: float = 0.2):
    """Removes values at random with probability `ratio`."""

    def _mcar(x: Union[int, float]):
        if np.random.uniform() < ratio:
            return np.nan
        else:
            return x

    return df.applymap(_mcar)


def missing_at_random(X, ratio=2):
    raise NotImplementedError


def self_masking(df: pd.DataFrame, thresh: float = 1.5):
    """Removes values that are more than `thresh` x std away from the mean.

    Args:
        df (pd.DataFrame): Dataframe whose (all) columns will be masked.
        thresh (float, optional): Threshold to flag outliers. Defaults to 1.5.

    Examples:
    >>> self_masking(pd.DataFrame({"X1": np.arange(10), "X2": np.arange(10)[::-1]})).head(3)
        X1   X2
    0  NaN  NaN
    1  1.0  8.0
    2  2.0  7.0
    """

    def _self_masking(X: Union[pd.Series, np.ndarray]):
        _std = np.std(X)
        _mean = np.mean(X)
        _out = np.abs(X - _mean) > thresh * _std
        X[_out] = np.nan
        return X

    return df.apply(_self_masking)
