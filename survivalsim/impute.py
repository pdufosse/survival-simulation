import numpy as np
from sklearn import impute as skimp

# allows import of IterativeImputer
from sklearn.experimental import enable_iterative_imputer
from sklearn.linear_model import BayesianRidge
from sklearn.pipeline import FeatureUnion
from sklearn.preprocessing import FunctionTransformer


class Imputer:
    def __init__(self, add_indicator=False) -> None:
        self.add_indicator = add_indicator
        if self.add_indicator:
            self.imp = FeatureUnion(
                [
                    ("impute", self.imp),
                    ("missing_indicator", skimp.MissingIndicator()),
                ]
            )
            self.__name__ += "_with_mask"
        else:
            self.imp = self.imp

    def fit(self, X):
        return self.imp.fit(X)

    def transform(self, X):
        return self.imp.transform(X)

    def fit_transform(self, X):
        return self.imp.fit_transform(X)


class GroundTruth(Imputer):
    __name__ = "ground_truth"
    imp = FunctionTransformer(lambda x: x)

    def __init__(self, add_indicator=False) -> None:
        if add_indicator:
            raise ValueError("`add_indicator=True` not supported for `GroundTruth`")
        super().__init__(add_indicator)


class SimpleImputer(Imputer):
    __name__ = "simple_impute"
    imp = skimp.SimpleImputer(missing_values=np.nan, strategy="median")


class MultipleImputer(Imputer):
    __name__ = "multiple_impute"
    imp = skimp.IterativeImputer(
        random_state=0, estimator=BayesianRidge(), max_iter=30, tol=1e-2
    )


class KNNImputer(Imputer):
    __name__ = "knn_impute"
    imp = skimp.KNNImputer(n_neighbors=10, weights="uniform")
