"""Functions to simulate survival dta

Author: Paul Dufossé
"""
from dataclasses import dataclass, field
from pathlib import Path
from typing import Callable, Optional, Union

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
import pandas as pd
import seaborn as sns
from lifelines import CoxPHFitter
from scipy.linalg import sqrtm
from sklearn.preprocessing import PolynomialFeatures

from survivalsim import plotting
from survivalsim.missing import missing_completely_at_random, self_masking
from survivalsim.utils import values_to_df


class BaseDesignSimulator:
    def generate_design_matrix(self, n):
        raise NotImplementedError


@dataclass
class BaseSimulator:
    """Base class to generate survival data

    Inherited simulators may or may not assume proportional hazards and/or non-informative censoring.

    Args:
        d(int): Number of covariates to generate.
        beta(_type_, optional): Effects of the Cox PH model. Defaults to None.
        wshape(int, optional): Shape parameter of the Weibull distribution. Defaults to 1.
        wscale(int, optional): Scale parameter of the Weibull distribution. Defaults to 100.
        REMOVED censoring_ratio(float, optional): Approximate number of censored events. Defaults to 0.2.
        xsim(BaseDesignSimulator): Design matrix simulator, see post init.
    """

    d: int
    beta: np.ndarray
    wshape: int = 1
    wscale: int = 100
    min_followup: int = 0
    max_followup: int = 150
    # censoring_ratio: float = .2
    msim: Callable = missing_completely_at_random
    xsim: BaseDesignSimulator = field(init=False)

    plot_survival: Callable = plotting.plot_survival
    plot_censorship: Callable = plotting.plot_censorship

    def __post_init__(self):
        self.xsim = TwoGroupsOneCovSimulator(self.d)

    def generate_missing_data(self, missing_ratio: float):
        raise NotImplementedError

    def generate(
        self, n: int, plot: bool = False, missing_params: Optional[dict] = None
    ):
        """Generate n, eventually censored, entries in the data

        Args:
            n (int): Number of samples to generate.
            plot (bool, optional): Plot the KM estimator. Defaults to False.
            missing_covariates (bool, optional): Generate mask of missing. Defaults to False.

        Returns:
            X, (T, D) (df, (array, array)): Design matrix, (times, event).
        """
        df = self.xsim.generate_design_matrix(n)
        Xset = [_ for _ in df.columns if _.startswith("x")]
        X = df[Xset].values
        Ts, Tc, f = self.compute_survival_times(X)
        df["f"] = f
        df["survival"] = Ts
        df["time"] = np.minimum(Ts, Tc)
        df["event"] = (Ts < Tc).astype(int)
        if plot:
            self.plot_survival(df)
        if missing_params is not None:
            df_full = df.copy()
            df[Xset] = self.msim(df[Xset], **missing_params)
        else:
            df_full = None
        return df, df_full

    def compute_hazard(self, X: np.ndarray):
        raise NotImplementedError

    def compute_survival_times(self, X: np.ndarray):
        f = self.compute_hazard(X)
        Ts = (np.random.weibull(self.wshape, size=X.shape[0]) * f * self.wscale).astype(
            int
        )
        # Tc = (np.random.weibull(self.wshape, size=X.shape[0])
        #       * f * self.wscale
        #       ).astype(int)
        # Tc = np.maximum(Tc, self.min_followup)
        Tc = np.random.uniform(
            self.min_followup, self.max_followup, size=X.shape[0]
        ).astype(int)
        return Ts, Tc, f

    def cox_analysis(
        self,
        X: np.ndarray,
        T: np.ndarray,
        C: np.ndarray,
        show_residuals_plots: bool = False,
    ):
        df = values_to_df(X, (T, C))
        self.cph = CoxPHFitter()
        self.cph.fit(df, "time", "event")
        self.cph.check_assumptions(df, show_plots=show_residuals_plots)
        self.cph.print_summary()
        plt.figure()
        self.cph.plot()
        return df

    def get_prefix(self):
        return self.__name__


@dataclass
class SimPH(BaseSimulator):
    __name__ = "ph_and_mcar"
    min_followup: int = 30

    def compute_hazard(self, X):
        assert len(self.beta) == self.d
        f = np.exp(-X @ self.beta / self.wshape)
        return f

    def cox_analysis(self, X, T, C):
        """Adds true coefficients to fitted Cox model plot."""
        df = super().cox_analysis(X, T, C)
        coefs = pd.DataFrame(self.cph.params_.copy())
        coefs["true"] = self.beta
        coefs = coefs.sort_values(by="coef")
        plt.scatter(
            coefs["true"],
            np.arange(self.d),
            color="red",
            marker="X",
            s=50,
            label=r"$\beta$",
        )
        plt.legend()
        return df


@dataclass
class SimNPH(BaseSimulator):
    __name__ = "nph_and_mcar"
    min_followup: int = 30

    def compute_hazard(self, X):
        assert len(self.beta) == self.d * (self.d + 1) / 2 + 1  # intercept term
        poly2 = PolynomialFeatures(2, interaction_only=2)
        _X_poly = poly2.fit_transform(X)
        f = np.exp(-_X_poly @ self.beta / self.wshape)
        return f


@dataclass
class SimPHMNAR(BaseSimulator):
    __name__ = "ph_and_mnar"
    min_followup: int = 30
    msim: Callable = self_masking

    def compute_hazard(self, X):
        assert len(self.beta) == self.d
        f = np.exp(-X @ self.beta / self.wshape)
        return f

    def cox_analysis(self, X, T, C):
        """Adds true coefficients to fitted Cox model plot."""
        df = super().cox_analysis(X, T, C)
        coefs = pd.DataFrame(self.cph.params_.copy())
        coefs["true"] = self.beta
        coefs = coefs.sort_values(by="coef")
        plt.scatter(
            coefs["true"],
            np.arange(self.d),
            color="red",
            marker="X",
            s=50,
            label=r"$\beta$",
        )
        plt.legend()
        return df


@dataclass
class SimNPHMNAR(BaseSimulator):
    __name__ = "nph_and_mnar"

    min_followup: int = 30
    msim: Callable = self_masking

    def compute_hazard(self, X):
        assert len(self.beta) == self.d * (self.d + 1) / 2 + 1  # intercept term
        poly2 = PolynomialFeatures(2, interaction_only=2)
        _X_poly = poly2.fit_transform(X)
        f = np.exp(-_X_poly @ self.beta / self.wshape)
        return f


def generate_covariance_matrix_naive(d, sqrt=True, unit=True):
    A = 1 + 10 * np.random.uniform(size=d**2).reshape(d, d)
    if unit:
        A /= np.linalg.norm(A)
    if sqrt:
        return A
    else:
        return A @ A.T


def generate_covariance_matrix(
    d, kappa: float = 100, sqrt: bool = True, check: bool = True, unit: bool = False
):
    assert kappa > 1
    sigmas = (1 + (kappa - 1) * np.random.uniform(size=d)).reshape(-1, 1)
    Q, _ = np.linalg.qr(np.random.normal(size=d**2).reshape(d, d))
    if sqrt:
        S = Q.T * np.sqrt(sigmas)
    else:
        S = Q.T @ (sigmas * Q)
    if check:
        if sqrt:
            _ev = np.linalg.eigvals(S.T @ S)
        else:
            _ev = np.linalg.eigvals(S)
        assert np.all(_ev > 0)
        assert _ev[0] / _ev[-1] < kappa
    if unit:
        S /= np.linalg.norm(S)
    return S


def generate_uniform(n: int, d: int):
    X = np.random.uniform(size=n * d).reshape(n, d)
    return X


def is_pos_def(A: np.matrix):
    return np.all(np.linalg.eigvals(A) > 0)


def is_symmetric(A: np.matrix):
    return A.T == A


def generate_normal(n: int, d: int, B=None):
    X = np.random.uniform(size=n * d).reshape(n, d)
    if B is not None:
        X = X @ B
    return X


def generate_mixture(
    n: int,
    d: int,
    p: float,
    mu1: np.ndarray,
    B1: np.matrix,
    mu2: np.ndarray,
    B2: np.matrix,
):
    G = np.random.binomial(1, p, size=n).reshape(-1, 1)
    assert n * (1 - p) * 2 > G.sum() > n * p / 2
    X = np.multiply(G, mu1 + generate_normal(n, d, B1)) + np.multiply(
        1 - G, mu2 + generate_normal(n, d, B2)
    )
    return X, G


def generate_random_vector(d, unit: bool = False):
    v = np.random.normal(size=d)
    if unit:
        v /= np.linalg.norm(v)
    return v


@dataclass
class TwoGroupsSimulator(BaseDesignSimulator):
    d: int
    p: float = 0.5

    def __post_init__(self):
        self.mu1 = generate_random_vector(self.d, unit=False)
        self.mu2 = generate_random_vector(self.d, unit=False)
        self.B1 = generate_covariance_matrix(self.d, unit=True)
        self.B2 = generate_covariance_matrix(self.d, unit=True)

    def generate_design_matrix(self, n):
        X, G = generate_mixture(n, self.d, self.p, self.mu1, self.B1, self.mu2, self.B2)
        df = pd.DataFrame(X, dtype=float)
        df = df.rename(columns={i: f"x{i}" for i in range(X.shape[1])})
        df["group"] = G
        return df

    def __repr__(self):
        import array_to_latex as a2l

        def info_means(mu1, mu2):
            print(
                f"""
    mu1 = {mu1}, mu2 = {mu2}
    norm(mu1 - mu2) = {np.linalg.norm(mu1 - mu2):.3f}
            """
            )

        def info_covariance_matrix(B, sqrt=True):
            if sqrt:
                B = B.T @ B
            a2l.to_ltx(B)
            print(np.linalg.cond(B))

        info_means(self.mu1, self.mu2)
        info_covariance_matrix(self.B1)
        info_covariance_matrix(self.B2)
        return ""


@dataclass
class TwoGroupsOneCovSimulator(TwoGroupsSimulator):
    d: int = 5
    p: float = 0.5
    rho: float = 0.5

    def __post_init__(self):
        self.mu1 = generate_random_vector(self.d)
        self.mu2 = generate_random_vector(self.d)
        B = sqrtm(
            np.matrix(
                [
                    [1, self.rho, self.rho, 0, 0],
                    [self.rho, 1, 0, 0, 0],
                    [self.rho, 0, 1, self.rho, 0],
                    [0, 0, self.rho, 1, self.rho],
                    [0, 0, 0, self.rho, 1],
                ]
            )
        )
        assert is_pos_def(B)

        self.B1 = B
        self.B2 = B

    def generate_design_matrix(self, n):
        X, G = generate_mixture(n, self.d, self.p, self.mu1, self.B1, self.mu2, self.B2)
        df = pd.DataFrame(X, dtype=float)
        df = df.rename(columns={i: f"x{i}" for i in range(X.shape[1])})
        df["group"] = G
        return df
