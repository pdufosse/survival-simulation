import pandas as pd
import numpy as np


def df_to_values(df, duration_col="time", event_col="event"):
    return df.values, df[duration_col].values, df[event_col].values


def values_to_df(X, y):
    """Converts array to df

    Args:
        X (matrix): Design matrix
        y (2-tuple of vectors): Label / outcome of time and events

    Returns:
        df (pandas.DataFrame): Dataset compatible with `lifelines` fitters.

    """
    df = pd.DataFrame(X, dtype=float)
    df["time"], df["event"] = y
    return df


def target_to_cv(T, C):
    return np.array(list(zip(T, C)))


def cv_to_target(y):
    return np.array(list(map(list, zip(*y))))


def cv_to_df(X, y):
    return values_to_df(X, cv_to_target(y))


def to_torch_float(X):
    return X.astype("float32")


def array_to_structured(y):
    # to sksurv format
    return np.array(
        [tuple(_) for _ in y[:, ::-1]],
        dtype=[('cens', '?'), ('time', '<f8')]
    )


def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]


def median(x):
    """Wrapper around np.median

    Returns nearest value in x
    """
    return find_nearest(x, np.median(x))


def dict_to_str(d):
    return "_".join([f"{k}={v:.2f}" for k, v in d.items()])
